#!/bin/bash
scripts="/home/adri/projects/licode/scripts"
if [ "$1" == "start" ]; then
	cd $scripts
	./initLicode.sh
elif [ "$1" == "stop" ]; then
	cd $scripts
	./stopLicode.sh
else
	echo "Bad parameter"
fi
