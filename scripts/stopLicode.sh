#!/bin/bash
pids=`pidof node`
array=($pids)
for pid in "${array[@]}"; do
	kill -9 $pid
done

echo "Licode terminated."
